export default class {
    private createRandomArray = (): number[] => {
        var arr: any[] = Array.apply(null, Array(Math.round(Math.random() * 31) + 10));
        const nmbrs: Array<number> = new Array<number>();
        arr.forEach(element => {
            nmbrs.push(Math.round(Math.random() * 101));
        });
        return nmbrs;
    }

    private slevani = (arr1: number[], arr2: number[]): number[] => {
        let res: number[] = new Array(arr1.length + arr2.length);
        let i = 0;
        let j = 0;

        while ((i < arr1.length) && (j < arr2.length)) {
            if (arr1[i] < arr2[j]) {
                res[i + j] = arr1[i];
                i++;
            }
            else if (arr1[i] >= arr2[j]) {
                res[i + j] = arr2[j];
                j++;
            }
        }

        if (i < arr1.length) {
            for (let k = i; k < arr1.length; k++) {
                res[k + j] = arr1[k];
            }
        }
        else if (j < arr2.length) {
            for (let k = j; k < arr2.length; k++) {
                res[k + i] = arr2[k];
            }
        }

        return res;
    }

    private printArray = (arr: number[]): void => {
        console.log(arr);
        console.log("-------");
    }

    /**
     * Main method
     */
    public execute = (): void => {
        const arr1: number[] = this.createRandomArray();
        const arr2: number[] = this.createRandomArray();
        const sortedArr1: number[] = arr1.sort((a: number, b: number) => a - b);
        const sortedArr2: number[] = arr2.sort((a: number, b: number) => a - b);
        this.printArray(arr1);
        this.printArray(arr2);
        console.log();
        this.printArray(this.slevani(arr1, arr2));
    }
}


